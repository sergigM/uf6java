create table Clients (
id_customer serial primary key,
	name varchar(255),
	mail varchar(255),
	adress varchar(255)
);

create table Pais(
id_country serial primary key,
	name varchar(255)
);

create table Productes(
id_product serial primary key ,
	name varchar(255),
	price varchar(255),
	code varchar(255),
	id_country int,
	foreign key (id_country) references pais (id_country)
);

create table Encarrec(
id_customer int,
	id_product int,
	state boolean,
	amount int,
	foreign key (id_customer) references Clients (id_customer),
	foreign key (id_product) references Productes (id_product),
);




insert into clients(name,mail,adress) values('Client1','client1@itb.cat','adreça1');
insert into clients(name,mail,adress) values('Client2','client2@itb.cat','adreça2');
insert into clients(name,mail,adress) values('Client3','client3@itb.cat','adreça3');

insert into pais(name) values('Esapnya');
insert into pais(name) values('Cuba');
insert into pais(name) values('França');
insert into pais(name) values('Marroc');

insert into productes(name,price,code,id_country) values('Arros',2,8401234646546,1);
insert into productes(name,price,code,id_country) values('Tabac',100,8502347456246,2);
insert into productes(name,price,code,id_country) values('Formatge',50,3005654984263,3);
insert into productes(name,price,code,id_country) values('Taronges',15,7294938206928,4);


/*
drop table encarrec;
drop table productes;
drop table pais;
drop table clients;
*/




