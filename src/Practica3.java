import java.sql.*;
import java.util.Scanner;

public class Practica3 {
    static Scanner lector = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {

        int option = 1;
        String usuari = "sergi-garriga-7e3";
        String clau = "ITBsergiGM";
        String urlDades = "jdbc:postgresql://postgresql-sergi-garriga-7e3.alwaysdata.net/sergi-garriga-7e3_bbdd";
        //Class.forName("org.postgresql.Driver");
        Connection conexio = DriverManager.getConnection(urlDades, usuari, clau);
        Statement pregunta = conexio.createStatement();
        while (option != 4) {
            System.out.println("1: Mostrar clientes por apellido \n2: Insertar cliente \n3: Eliminar cliente \n4: Salir");
            option = lector.nextInt();

            switch (option) {
                case 1:
                    Mostrar(pregunta);
                    break;
                case 2:
                    Insert(conexio);
                    break;
                case 3:
                    Delete(conexio);
                    break;
                case 4:
                    option = 4;
                    break;
            }
        }
    }

    public static void Delete(Connection conexio) throws SQLException {
        System.out.println("id");
        int id = lector.nextInt();

        PreparedStatement statementPedido = conexio.prepareStatement("DELETE FROM pedido WHERE id_cliente=?");
        statementPedido.setInt(1, id);
        PreparedStatement statement = conexio.prepareStatement("DELETE FROM cliente WHERE id=?");
        statement.setInt(1, id);

        statementPedido.executeUpdate();
        statement.executeUpdate();
    }

    public static void Insert(Connection conexio) throws SQLException {
        lector.nextLine();
        System.out.println("ID manual?");
        String manual = lector.nextLine();

        System.out.println("nom");
        String nom = lector.nextLine();

        System.out.println("congom");
        String cognom = lector.nextLine();

        System.out.println("cognom2");
        String cognom2 = lector.nextLine();

        System.out.println("ciutat");
        String ciutat = lector.nextLine();

        System.out.println("categoria");
        int categoria = lector.nextInt();

        try {
            if (manual.equalsIgnoreCase("yes") || manual.equalsIgnoreCase("y") || manual.equalsIgnoreCase("s") || manual.equalsIgnoreCase("si")) {
                System.out.println("id");
                int id = lector.nextInt();
                PreparedStatement statement = conexio.prepareStatement("Insert INTO cliente  values(?,?,?,?,?,?)");
                statement.setInt(1, id);

                statement.setString(2, nom);
                statement.setString(3, cognom);
                statement.setString(4, cognom2);
                statement.setString(5, ciutat);
                statement.setInt(6, categoria);
                statement.executeUpdate();

            } else {

                PreparedStatement statement = conexio.prepareStatement("Insert INTO cliente  values(DEFAULT ,?,?,?,?,?)");

                statement.setString(1, nom);
                statement.setString(2, cognom);
                statement.setString(3, cognom2);
                statement.setString(4, ciutat);
                statement.setInt(5, categoria);
                statement.executeUpdate();

            }
        } catch (SQLException e) {
            if (e instanceof org.postgresql.util.PSQLException) {
                System.out.println("ID Duplicada");
                System.out.println("\n");
            }
        }


        //System.out.println(id + " " + nom + " " + cognom + " " + cognom2 + " " + ciutat + " " + categoria);

    }

    public static void Mostrar(Statement pregunta) throws SQLException {
        ResultSet resposta = pregunta.executeQuery("SELECT * FROM cliente order by 3");
        System.out.println("\n");
        while (resposta.next()) {
            int id = resposta.getInt("id");
            String nom = resposta.getString("nombre");
            String cognom = resposta.getString("apellido1");
            String cognom2 = resposta.getString("apellido2");
            String ciutat = resposta.getString("ciudad");
            int categoria = resposta.getInt("categoría");

            System.out.println(id + " " + nom + " " + cognom + " " + cognom2 + " " + ciutat + " " + categoria);
        }
        System.out.println("\n");
    }
}