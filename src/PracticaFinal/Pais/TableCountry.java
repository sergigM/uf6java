package PracticaFinal.Pais;


import PracticaFinal.Pais.CountrySearch;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static PracticaFinal.BBDD.*;
import static PracticaFinal.TabbedPaneTables.numFila;

public class TableCountry extends JPanel {
    public static DefaultTableModel modelTableCountry = new DefaultTableModel();

    public static JButton buttonOrder = new JButton("Buscar");
    public static JButton buttonOrderReset = new JButton("Reset/Update");

    public TableCountry() {

        setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
        final String[] nomCol = {"Pais", "Producte"};

        for (int i = 0; nomCol.length > i; i++) {
            modelTableCountry.addColumn(nomCol[i]);
        }
        JTable table = new JTable(modelTableCountry);
        JScrollPane scroll = new JScrollPane(table);
        add(scroll);

        JPanel panelButton = new JPanel();
        panelButton.setBorder(new EmptyBorder(2, 50, 2, 50));
        buttonOrder.addActionListener(actionEvent -> {
            CountrySearch newOrder = new CountrySearch();
            newOrder.setVisible(true);
        });
        panelButton.add(buttonOrder);

        buttonOrderReset.addActionListener(actionEvent -> {
            try {
                reset();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        panelButton.add(buttonOrderReset);
        add(panelButton);
        try {

            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            reset();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.addListSelectionListener(e -> {
            //int fila = table.getSelectedRow(), columna = 0;
            numFila = table.getSelectedRow();
            //String selectedItem = (String) table.getValueAt(fila, columna);
            //idSeleccionat= Integer.parseInt(selectedItem);
        });


        modelTableCountry.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent tableModelEvent) {
                if (tableModelEvent.getType() == TableModelEvent.UPDATE) {
                    try {
                        PreparedStatement statement = conexio.prepareStatement("update pais set name=? where id_country=?");
                        statement.setString(1, table.getValueAt(numFila, 1).toString());
                        statement.setInt(2, Integer.parseInt(table.getValueAt(numFila, 0).toString()));
                        statement.executeUpdate();

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });



    }

    public static void reset() throws SQLException {
        modelTableCountry.setRowCount(0);
        ResultSet resp = pregunta.executeQuery("select pais.name as pais,productess.name as producte from pais inner join productess on pais.id_country=productess.id_country order by pais.id_country");

        while (resp.next()) {
            String id = resp.getString("pais");
            String name = resp.getString("producte");

            String[] strArray = {id, name};
            modelTableCountry.addRow(strArray);
            //System.out.println(id + " " + name + " " + price + " " + code + " " + id_country);
        }
    }
}
