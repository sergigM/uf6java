package PracticaFinal.Pais;

import PracticaFinal.comboitem.ComboItem;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static PracticaFinal.BBDD.*;
import static PracticaFinal.BBDD.pregunta;
import static PracticaFinal.Pais.TableCountry.modelTableCountry;

public class CountrySearch extends JDialog {

    private  ArrayList<String> pais = new ArrayList<String>();
    private  ArrayList<String> paisid = new ArrayList<String>();


    public CountrySearch() {
        setLayout(new FlowLayout());
        getCountry();
        getCountryId();
        JPanel panel = new JPanel();


        JComboBox paiss = new JComboBox();

        for (int i = 0; i < pais.size(); i++) {
            ComboItem item = new ComboItem(paisid.get(i), pais.get(i));
            paiss.addItem(item);

        }
        panel.add(paiss);
        add(panel);


        JPanel button = new JPanel();
        button.setLayout(new GridLayout(1, 2, 10, 10));
        button.setBorder(new EmptyBorder(2, 2, 2, 2));

        JButton ok = new JButton("Ok");
        JButton noOk = new JButton("Cancelar");
        button.add(ok);
        button.add(noOk);
        ok.addActionListener(actionEvent -> {
            try {
                ComboItem aaa = (ComboItem) paiss.getSelectedItem();
                getProducts(aaa.getValue());
                //System.out.println(paiss.getSelectedItem());
                this.dispose();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });

        noOk.addActionListener(actionEvent -> {
            this.dispose();
        });

        add(button);


        setSize(250, 400);

        setVisible(true);


    }

    public void getCountry() {
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select id_country,name from pais order by 1");

            while (resp.next()) {
                String name = resp.getString("name");
                pais.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void getCountryId() {
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select id_country from pais order by 1");

            while (resp.next()) {
                String name = resp.getString("id_country");
                paisid.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void getProducts(String paiss) throws SQLException {
        modelTableCountry.setRowCount(0);
        String[] info = new String[2];
        PreparedStatement statement = conexio.prepareStatement("select pais.name as pais,productess.name as producte from pais inner join productess on pais.id_country=productess.id_country where pais.id_country=?");
        statement.setInt(1, Integer.parseInt(paiss));
        ResultSet result = statement.executeQuery();

        while (result.next()) {
            String pais = result.getString("pais");
            String prod = result.getString("producte");
            info[0] = pais;
            info[1] = prod;
            modelTableCountry.addRow(info);
        }
    }
}
