package PracticaFinal.Orders;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import static PracticaFinal.BBDD.*;
import static PracticaFinal.TabbedPaneTables.idSeleccionat;
import static PracticaFinal.TabbedPaneTables.numFila;

public class TableOrders extends JPanel {
    public static DefaultTableModel modelTableEncarrec = new DefaultTableModel();
    public static JButton buttonNewOrder = new JButton("New Order");
    public static JButton buttonState = new JButton("Change State");
    public static  JTable table = new JTable(modelTableEncarrec);

    private static int prev_cust, prev_prod;


    public TableOrders() {


        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));


        final String[] nomCol = {"id_customer", "id_product", "state", "amount"};

        for (int i = 0; nomCol.length > i; i++) {
            modelTableEncarrec.addColumn(nomCol[i]);
        }

        JScrollPane scroll = new JScrollPane(table);
        add(scroll);


        JPanel panelButton = new JPanel();
        panelButton.setBorder(new EmptyBorder(2, 50, 2, 50));
        buttonNewOrder.addActionListener(actionEvent -> {
            NewOrder newOrder = new NewOrder();
            newOrder.setVisible(true);
        });
        panelButton.add(buttonNewOrder);

        buttonState.addActionListener(actionEvent -> {
            try {
                changeState();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        });
        panelButton.add(buttonState);

        add(panelButton);

        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            //ResultSet resp = pregunta.executeQuery("select clients.name as client, productes.name as producte, order_state, amount from encarrec inner join clients on encarrec.id_customer=clients.id_customer inner join productes on encarrec.id_product=productes.id_product");
            ResultSet resp = pregunta.executeQuery("select * from encarrec order by 1;");

            while (resp.next()) {
                String id_customer = resp.getString("id_customer");
                String id_product = resp.getString("id_product");
                boolean state = resp.getBoolean("state");
                int amount = resp.getInt("amount");
                Boolean servit = true;

                if (!state) {
                    servit = false;
                }


                String[] strArray = {id_customer, id_product, String.valueOf(servit), Integer.toString(amount)};
                modelTableEncarrec.addRow(strArray);
                //System.out.println(id + " " + name + " " + price + " " + code + " " + id_country);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }



        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.addListSelectionListener(e -> {

            numFila = table.getSelectedRow();

            if (numFila==-1){
                numFila=0;
            }
            int columna = 0;
            String selectedItem = (String) table.getValueAt(numFila, columna);
            idSeleccionat= Integer.parseInt(selectedItem);
            System.out.println(numFila);




            String selectIdProd = (String) table.getValueAt(numFila, 1);
            prev_prod = Integer.parseInt(selectIdProd);
            String selectIdCust = (String) table.getValueAt(numFila, 0);
            prev_cust = Integer.parseInt(selectIdCust);
        });


        modelTableEncarrec.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent tableModelEvent) {
                if (tableModelEvent.getType() == TableModelEvent.UPDATE) {
                    try {
                        PreparedStatement statement = conexio.prepareStatement("update encarrec set id_customer=?,id_product=?,state=?,amount=? where id_product=? and id_customer=?");

                        statement.setInt(1, Integer.parseInt(table.getValueAt(numFila, 0).toString()));
                        statement.setInt(2, Integer.parseInt(table.getValueAt(numFila, 1).toString()));
                        statement.setBoolean(3, Boolean.parseBoolean(table.getValueAt(numFila, 2).toString()));
                        statement.setInt(4, Integer.parseInt(table.getValueAt(numFila, 3).toString()));
                        statement.setInt(6, (prev_cust));
                        statement.setInt(5, (prev_prod));
                        statement.executeUpdate();

                        //per seguir modificant la mateixa fila si es canvien les id
                        String selectIdProd = (String) table.getValueAt(numFila, 1);
                        prev_prod = Integer.parseInt(selectIdProd);
                        String selectIdCust = (String) table.getValueAt(numFila, 0);
                        prev_cust = Integer.parseInt(selectIdCust);

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });
    }
    public void changeState() throws SQLException {

        String state =table.getValueAt(numFila, 2).toString();

        if (state.equalsIgnoreCase("true")){
            state="false";
        }else{
            state="true";
        }

        PreparedStatement statement = conexio.prepareStatement("update encarrec set state=? where id_product=? and id_customer=? and amount=? and state=?");

        statement.setBoolean(1, Boolean.getBoolean(state));
        statement.setInt(2, (prev_prod));
        statement.setInt(3, (prev_cust));
        statement.setInt(4, Integer.parseInt(table.getValueAt(numFila,3).toString()));
        statement.setBoolean(5, Boolean.parseBoolean(table.getValueAt(numFila,2).toString()));
        statement.executeUpdate();
        table.setValueAt(state,numFila,2);

    }

}
