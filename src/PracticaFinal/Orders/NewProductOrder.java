package PracticaFinal.Orders;

import PracticaFinal.comboitem.ComboItem;

import javax.swing.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static PracticaFinal.BBDD.*;

public class NewProductOrder extends JComboBox {
    private ArrayList<String> product = new ArrayList<String>();
    private ArrayList<String> productID = new ArrayList<String>();


    public NewProductOrder(){
        getProducts();
        getProductsId();

        for (int i = 0; i < product.size(); i++) {
            ComboItem item =new ComboItem(productID.get(i),product.get(i));
            this.addItem(item);
        }
    }

    public void getProducts(){
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select * from productess order by 1");

            while (resp.next()) {
                String name = resp.getString("name");
                product.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void getProductsId(){
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select id_product from productess order by 1");

            while (resp.next()) {
                String name = resp.getString("id_product");
                productID.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
