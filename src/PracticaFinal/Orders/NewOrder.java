package PracticaFinal.Orders;

import PracticaFinal.comboitem.ComboItem;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;

import static PracticaFinal.BBDD.*;
import static PracticaFinal.Customer.TableCustomer.modelTableCustomer;
import static PracticaFinal.TabbedPaneTables.numFila;
import static PracticaFinal.Orders.TableOrders.modelTableEncarrec;

public class NewOrder extends JDialog {
    private JPanel finestraAfegir = new JPanel();
    private ArrayList<String> customers = new ArrayList<String>();
    private ArrayList<String> customersId = new ArrayList<String>();


    public NewOrder() {
        getCustomers();
        getCustomersId();
        String[] dades = new String[4];

        finestraAfegir.setLayout(new GridLayout(4, 1, 1, 10));

        JComboBox customer = new JComboBox();
        for (int i = 0; i < customers.size(); i++) {
            ComboItem item =new ComboItem(customersId.get(i),customers.get(i));
            customer.addItem(item);


        }


        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(0);
        formatter.setMaximum(Integer.MAX_VALUE);
        formatter.setAllowsInvalid(false);
        formatter.setCommitsOnValidEdit(true);

        JPanel client = new JPanel();


        client.add(customer);
        finestraAfegir.add(client);

        JPanel products = new JPanel();
        products.setLayout(new BoxLayout(products, BoxLayout.Y_AXIS));


        NewProductOrder prod1 =new NewProductOrder();


        JFormattedTextField amou1 = new JFormattedTextField(formatter);


        products.add(prod1);
        products.add(amou1);


        finestraAfegir.add(products);


        JPanel button = new JPanel();
        button.setLayout(new GridLayout(1, 2, 10, 10));
        button.setBorder(new EmptyBorder(2, 2, 2, 2));

        JButton ok = new JButton("Ok");
        JButton noOk = new JButton("Cancelar");
        button.add(ok);
        button.add(noOk);
        ok.addActionListener(actionEvent -> {

            ComboItem cust = (ComboItem)customer.getSelectedItem();
            ComboItem prod = (ComboItem)prod1.getSelectedItem();

            System.out.println(cust.getValue());
            System.out.println(prod.getValue());


            System.out.println(amou1.getText());

            String amount =String.valueOf(amou1.getText());
            amount=amount.replace(".","");
            addOrder(cust,prod,amount);



            dades[0] = cust.getValue();
            dades[1] = prod.getValue();
            dades[2] = "false";
            dades[3] = amount;




            modelTableEncarrec.addRow(dades);

            this.dispose();

        });

        noOk.addActionListener(actionEvent -> {
            this.dispose();
        });

        finestraAfegir.add(button);




        setSize(250, 400);
        finestraAfegir.setVisible(true);
        add(finestraAfegir);
        setVisible(true);


    }

    public static void main(String[] args) {
        new NewOrder();
    }

    public void getCustomers() {
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select * from clients order by 1");

            while (resp.next()) {
                String name = resp.getString("name");
                customers.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    public void getCustomersId() {
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select id_customer from clients order by 1");

            while (resp.next()) {
                String name = resp.getString("id_customer");
                customersId.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }


    public void addOrder(ComboItem cust, ComboItem prod, String amou1) {
        try {
            PreparedStatement statement = conexio.prepareStatement("insert into encarrec(id_customer,id_product,state,amount) values (?,?,?,?);");

            statement.setInt(1, Integer.parseInt(cust.getValue()));
            statement.setInt(2, Integer.parseInt(prod.getValue()));
            statement.setBoolean(3, false);


            statement.setInt(4, Integer.parseInt(amou1));
            statement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}