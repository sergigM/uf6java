package PracticaFinal;

import PracticaFinal.Customer.TableCustomer;
import PracticaFinal.Orders.TableOrders;
import PracticaFinal.Pais.TableCountry;
import PracticaFinal.Productos.TableProducts;

import javax.swing.*;

public class TabbedPaneTables extends JTabbedPane {
    public static int idSeleccionat,numFila;

    public TabbedPaneTables(){
        addTab("Customer",new TableCustomer());
        addTab("Pais",new TableCountry());
        addTab("Encarrecs",new TableOrders());
        addTab("Productos",new TableProducts());

    }
}
