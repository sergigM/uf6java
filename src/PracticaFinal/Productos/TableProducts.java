package PracticaFinal.Productos;


import PracticaFinal.Pais.TableCountry;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import static PracticaFinal.BBDD.*;
import static PracticaFinal.TabbedPaneTables.idSeleccionat;
import static PracticaFinal.TabbedPaneTables.numFila;

public class TableProducts extends JPanel {
    public static DefaultTableModel modelTableProducts = new DefaultTableModel();

    public static JButton buttonAdd = new JButton("Add");
    public static JButton buttonDelete = new JButton("Delete");

    public TableProducts() {

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));


        final String[] nomCol = {"id_product", "name", "price", "code", "id_country"};

        for (int i = 0; nomCol.length > i; i++) {
            modelTableProducts.addColumn(nomCol[i]);
        }
        JTable table = new JTable(modelTableProducts);
        JScrollPane scroll = new JScrollPane(table);
        add(scroll);


        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select * from productess order by 1");

            while (resp.next()) {
                int id = resp.getInt("id_product");
                String name = resp.getString("name");
                String price = resp.getString("price");
                String code = resp.getString("code");
                int id_country = resp.getInt("id_country");


                String[] strArray = {Integer.toString(id), name, price, code, Integer.toString(id_country)};
                modelTableProducts.addRow(strArray);
                //System.out.println(id + " " + name + " " + price + " " + code + " " + id_country);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.addListSelectionListener(e -> {
            int columna = 0;

            numFila = table.getSelectedRow();
            if (numFila==-1){
                numFila=0;
            }
            String selectedItem = (String) table.getValueAt(numFila, columna);
            idSeleccionat= Integer.parseInt(selectedItem);
        });


        modelTableProducts.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent tableModelEvent) {
                if (tableModelEvent.getType() == TableModelEvent.UPDATE) {
                    try {
                        PreparedStatement statement = conexio.prepareStatement("update productes set name=?,price=?,code=?,id_country=? where id_product=?");
                        statement.setString(1, table.getValueAt(numFila, 1).toString());
                        statement.setString(2, (table.getValueAt(numFila, 2).toString()));
                        statement.setString(3, (table.getValueAt(numFila, 3).toString()));
                        statement.setInt(4, Integer.parseInt(table.getValueAt(numFila, 4).toString()));
                        statement.setInt(5, Integer.parseInt(table.getValueAt(numFila, 0).toString()));
                        statement.executeUpdate();

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });

        JPanel panelButton = new JPanel();
        panelButton.setBorder(new EmptyBorder(2, 50, 2, 50));
        buttonAdd.addActionListener(actionEvent -> {
            try {
                NewProduct add = new NewProduct();
                add.setVisible(true);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        panelButton.add(buttonAdd);

        buttonDelete.addActionListener(actionEvent -> {
            try {
                delete(idSeleccionat);


            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        });
        panelButton.add(buttonDelete);

        add(panelButton);


    }

    public void delete(int idSeleccionat) throws SQLException {
        modelTableProducts.removeRow(numFila);
        numFila = 0;


        PreparedStatement statementProductes = conexio.prepareStatement("DELETE FROM productess WHERE id_product=?");
        statementProductes.setInt(1, idSeleccionat);
        statementProductes.executeUpdate();

    }
}
