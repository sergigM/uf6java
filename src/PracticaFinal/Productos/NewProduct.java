package PracticaFinal.Productos;

import PracticaFinal.comboitem.ComboItem;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static PracticaFinal.BBDD.*;
import static PracticaFinal.Productos.TableProducts.modelTableProducts;


public class NewProduct extends JDialog {
    private ArrayList<String> pais = new ArrayList<String>();
    private  ArrayList<String> paisid = new ArrayList<String>();
    public NewProduct() throws SQLException {
        conexio = DriverManager.getConnection(urlDades, usuari, clau);
        pregunta = conexio.createStatement();
        getCountry();
        getCountryId();

        String[] dades = new String[5];


        JPanel finestraAfegir = new JPanel();
        finestraAfegir.setLayout(new GridLayout(10, 1, 1, 10));
        finestraAfegir.setBorder(new EmptyBorder(10, 10, 10, 10));
        this.setSize(250, 400);

        finestraAfegir.add(new JLabel("Nom"));
        JTextField nom = new JTextField("", 20);
        finestraAfegir.add(nom);

        finestraAfegir.add(new JLabel("Preu"));
        JTextField marca = new JTextField("", 20);
        finestraAfegir.add(marca);

        finestraAfegir.add(new JLabel("Codi"));
        JTextField preu = new JTextField("", 20);
        finestraAfegir.add(preu);

        finestraAfegir.add(new JLabel("Pais"));
        JComboBox paiss = new JComboBox();

        for (int i = 0; i < pais.size(); i++) {
            ComboItem item = new ComboItem(paisid.get(i), pais.get(i));
            paiss.addItem(item);

        }
        finestraAfegir.add(paiss);


        JPanel button = new JPanel();
        button.setLayout(new GridLayout(1, 2, 10, 10));
        button.setBorder(new EmptyBorder(2, 2, 2, 2));

        JButton ok = new JButton("Ok");
        JButton noOk = new JButton("Cancelar");
        button.add(ok);
        button.add(noOk);
        ok.addActionListener(actionEvent -> {
            try {

                ComboItem aaa = (ComboItem) paiss.getSelectedItem();

                dades[1] = nom.getText();
                dades[2] = marca.getText();
                dades[3] = preu.getText();
                dades[4] = aaa.getValue();
                System.out.println(aaa.getValue());
                //System.out.println(dades[0]+" "+dades[1]+" "+dades[2]+" "+dades[3]+" "+dades[4]);
                PreparedStatement statement = conexio.prepareStatement("Insert INTO PRODUCTESS values(DEFAULT ,?,?,?,?)");

                statement.setString(1, dades[1]);
                statement.setString(2, dades[2]);
                statement.setString(3, dades[3]);
                statement.setInt(4, Integer.parseInt(dades[4]));


                statement.executeUpdate();

                dades[0] = String.valueOf(lastId());



                modelTableProducts.addRow(dades);
                this.dispose();

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

        });

        noOk.addActionListener(actionEvent -> {
            this.dispose();
        });

        finestraAfegir.add(button);
        finestraAfegir.setVisible(true);
        this.add(finestraAfegir);

    }

    public int lastId() throws SQLException {
        pregunta = conexio.createStatement();
        ResultSet resp = pregunta.executeQuery("Select max(id_product) from PRODUCTESS");
        if (resp.next()) {
            return resp.getInt("max");
        } else {
            return 0;
        }
    }

    public void getCountry() {
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select id_country,name from pais order by 1");

            while (resp.next()) {
                String name = resp.getString("name");
                pais.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void getCountryId() {
        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select id_country from pais order by 1");

            while (resp.next()) {
                String name = resp.getString("id_country");
                paisid.add(name);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}

