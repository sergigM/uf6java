package PracticaFinal.Customer;




import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import static PracticaFinal.BBDD.*;
import static PracticaFinal.TabbedPaneTables.numFila;
import static PracticaFinal.TabbedPaneTables.idSeleccionat;

public class TableCustomer extends JPanel {
    public static DefaultTableModel modelTableCustomer = new DefaultTableModel();

    public static JButton buttonAdd = new JButton("Add");
    public static JButton buttonDelete = new JButton("Delete");


    public TableCustomer() {

        setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));


        final String[] nomCol = {"id_customer", "name", "mail", "adress"};

        for (int i = 0; nomCol.length > i; i++) {
            modelTableCustomer.addColumn(nomCol[i]);
        }
        JTable table = new JTable(modelTableCustomer);
        JScrollPane scroll = new JScrollPane(table);
        add(scroll);




        JPanel panelButton = new JPanel();
        panelButton.setBorder(new EmptyBorder(2, 50, 2, 50));
        buttonAdd.addActionListener(actionEvent -> {
            try {
               NewCustomer add= new NewCustomer();
               add.setVisible(true);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        panelButton.add(buttonAdd);

        buttonDelete.addActionListener(actionEvent -> {


            try {
                delete(idSeleccionat);


            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        panelButton.add(buttonDelete);

        add(panelButton);




        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(urlDades, usuari, clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select * from clients order by 1");

            while (resp.next()) {
                int id = resp.getInt("id_customer");
                String name = resp.getString("name");
                String mail = resp.getString("mail");
                String adress = resp.getString("adress");


                String[] strArray = {Integer.toString(id), name, mail,adress};
                modelTableCustomer.addRow(strArray);
                //System.out.println(id + " " + name + " " + price + " " + code + " " + id_country);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }








        modelTableCustomer.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent tableModelEvent) {

                if (tableModelEvent.getType()== TableModelEvent.UPDATE){
                    try {
                        PreparedStatement statement = conexio.prepareStatement("update clients set name=?,mail=?,adress=? where id_customer=?");
                        statement.setString(1, table.getValueAt(numFila, 1).toString());
                        statement.setString(2, (table.getValueAt(numFila, 2).toString()));
                        statement.setString(3, (table.getValueAt(numFila, 3).toString()));
                        statement.setInt(4, Integer.parseInt(table.getValueAt(numFila, 0).toString()));

                        statement.executeUpdate();

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }else if (tableModelEvent.getType()== TableModelEvent.DELETE){


                }

            }
        });

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.addListSelectionListener(e -> {

            numFila = table.getSelectedRow();
            if (numFila==-1){
                numFila=0;
            }
            int columna = 0;
            String selectedItem = (String) table.getValueAt(numFila, columna);
            idSeleccionat= Integer.parseInt(selectedItem);
            System.out.println(numFila);
        });


    }
    public void delete(int idSeleccionat) throws SQLException {
        modelTableCustomer.removeRow(numFila);
        numFila=0;

        PreparedStatement statementProductes = conexio.prepareStatement("DELETE FROM clients WHERE id_customer=?");
        statementProductes.setInt(1, idSeleccionat);
        statementProductes.executeUpdate();

    }
}
