package PracticaFinal.Customer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static PracticaFinal.BBDD.*;
import static PracticaFinal.BBDD.conexio;
import static PracticaFinal.Customer.TableCustomer.modelTableCustomer;


public class NewCustomer extends JDialog {
    public NewCustomer() throws SQLException {
        conexio = DriverManager.getConnection(urlDades, usuari, clau);
        pregunta = conexio.createStatement();

        String[] dades = new String[4];


        JPanel finestraAfegir = new JPanel();
        finestraAfegir.setLayout(new GridLayout(10, 1, 1, 10));
        finestraAfegir.setBorder(new EmptyBorder(10, 10, 10, 10));
        this.setSize(250, 400);

        finestraAfegir.add(new JLabel("Nom"));
        JTextField nom = new JTextField("", 20);
        finestraAfegir.add(nom);

        finestraAfegir.add(new JLabel("Mail"));
        JTextField marca = new JTextField("", 20);
        finestraAfegir.add(marca);

        finestraAfegir.add(new JLabel("Adress"));
        JTextField preu = new JTextField("", 20);
        finestraAfegir.add(preu);


        JPanel button = new JPanel();
        button.setLayout(new GridLayout(1, 2, 10, 10));
        button.setBorder(new EmptyBorder(2, 2, 2, 2));

        JButton ok = new JButton("Ok");
        JButton noOk = new JButton("Cancelar");
        button.add(ok);
        button.add(noOk);
        ok.addActionListener(actionEvent -> {
            try {
                dades[1] = nom.getText();
                dades[2] = marca.getText();
                dades[3] = preu.getText();

                //System.out.println(dades[0]+" "+dades[1]+" "+dades[2]+" "+dades[3]+" "+dades[4]);


                PreparedStatement statement = conexio.prepareStatement("Insert INTO clients values(DEFAULT ,?,?,?)");

                statement.setString(1, dades[1]);
                statement.setString(2, dades[2]);
                statement.setString(3, (dades[3]));

                statement.executeUpdate();

                dades[0] = String.valueOf(lastId());

                modelTableCustomer.addRow(dades);
                this.dispose();

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

        });

        noOk.addActionListener(actionEvent -> {
            this.dispose();
        });

        finestraAfegir.add(button);
        finestraAfegir.setVisible(true);
        this.add(finestraAfegir);

    }

    public int lastId() throws SQLException {
        pregunta = conexio.createStatement();
        ResultSet resp = pregunta.executeQuery("Select max(id_customer) from clients");
        if (resp.next()) {
            return resp.getInt("max");
        } else {
            return 0;
        }
    }
}

