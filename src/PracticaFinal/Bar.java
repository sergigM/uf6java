package PracticaFinal;


import javax.swing.*;
import java.awt.*;

public class Bar extends JFrame {

    public static CardLayout cardLayout = new CardLayout();
    public static JPanel panelTables = new JPanel();

    public Bar() {
        super("Welcome to BAR");

        panelTables.setLayout(cardLayout);

        panelTables.add(new TabbedPaneTables());

        add(panelTables, BorderLayout.CENTER);


        setSize(900, 500);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        new Bar();
    }
}
