package CRUD;


import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class practica4 extends JFrame {

    public practica4() throws IOException {
        super("Practica 4");
        JPanel jPanelNorth = new JPanel();
        jPanelNorth.setSize(200, 200);
        jPanelNorth.add(new BarraAddDelete());

        add(jPanelNorth, BorderLayout.NORTH);
        Tabla tabla =new Tabla();
        add(tabla);
        setSize(900, 500);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) throws IOException {
        new practica4();
    }
}
