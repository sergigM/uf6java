package CRUD;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BarraAddDelete extends JToolBar implements ActionListener {
    public static JButton buttonAfegir, buttonEliminar;
    //menu con botones añadir y borrar
    public BarraAddDelete() throws IOException {


        ImageIcon icono = new ImageIcon(ImageIO.read(new File("./src/CRUD/img/afegir.png")).getScaledInstance(40, 40, Image.SCALE_SMOOTH));
        buttonAfegir = new JButton("Afegir", icono);
        buttonAfegir.addActionListener(this);
        add(buttonAfegir);


        icono = new ImageIcon(ImageIO.read(new File("./src/CRUD/img/borrar.png")).getScaledInstance(40, 40, Image.SCALE_SMOOTH));
        buttonEliminar = new JButton("Eliminar", icono);
        buttonEliminar.addActionListener(this);
        add(buttonEliminar);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonEliminar) {

            Tabla.modelTable.removeRow(Tabla.numFila);
            try {
                Delete(Tabla.idSeleccionat);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }


        } else if (e.getSource() == buttonAfegir) {
            Agregar agregar = new Agregar();
            agregar.setVisible(true);
        }
    }

    public static void Delete(int idSeleccionat) throws SQLException {

        PreparedStatement statementProductes = Tabla.conexio.prepareStatement("DELETE FROM productes WHERE id_producte=?");
        statementProductes.setInt(1, idSeleccionat);

        statementProductes.executeUpdate();
    }
}


