package CRUD;


import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.sql.*;


public class Tabla extends JPanel{
    public static DefaultTableModel modelTable;
    public static int idSeleccionat,numFila;
    public static  Connection conexio;
    public static  Statement pregunta;
    public static JTable table;



    public Tabla()  {
        final String[] nomCol = {"id", "Nombre", "Marca", "Preu", "PVP", "Imatge"};
        final Object[][] dades = {};

        modelTable = new DefaultTableModel(dades, nomCol);
        table = new JTable(modelTable);
        JScrollPane scroll = new JScrollPane(table);
        add(scroll);



        try {
            Class.forName("org.postgresql.Driver");
            conexio = DriverManager.getConnection(BBDD.urlDades, BBDD.usuari, BBDD.clau);
            pregunta = conexio.createStatement();
            ResultSet resp = pregunta.executeQuery("select * from productes order by id_producte");

            while (resp.next()) {
                int id = resp.getInt("id_producte");
                String nom = resp.getString("nom");
                String marca = resp.getString("marca");
                int preu = resp.getInt("preu");
                int pvp = resp.getInt("pvp");
                String imatge = resp.getString("img");

                String[] strArray = {Integer.toString(id), nom, marca, Integer.toString(preu), Integer.toString(pvp), imatge};
                modelTable.addRow(strArray);

                //System.out.println(id + " " + nom + " " + cognom + " " + cognom2 + " " + ciutat + " " + categoria);

            }

            modelTable.addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent tableModelEvent) {
                    try {
                        PreparedStatement statement = conexio.prepareStatement("update productes set nom=?,marca=?,preu=?,pvp=?,img=? where id_producte=?");
                        statement.setString(1, table.getValueAt(numFila, 1).toString());
                        statement.setString(2, table.getValueAt(numFila, 2).toString());
                        statement.setInt(3, Integer.parseInt(table.getValueAt(numFila, 3).toString()));
                        statement.setInt(4, Integer.parseInt(table.getValueAt(numFila, 4).toString()));
                        statement.setString(5, table.getValueAt(numFila, 5).toString());
                        statement.setInt(6, Integer.parseInt(table.getValueAt(numFila, 0).toString()));
                        statement.executeUpdate();

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }

                }
            });



            ListSelectionModel selectionModel = table.getSelectionModel();
            selectionModel.addListSelectionListener(e -> {
                int fila = table.getSelectedRow(), columna = 0;
                numFila = table.getSelectedRow();
                String selectedItem = (String) table.getValueAt(fila, columna);

                idSeleccionat= Integer.parseInt(selectedItem);
                System.out.println(numFila);
                System.out.println(idSeleccionat);
            });


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}